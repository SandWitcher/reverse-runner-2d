﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomSceneManager : MonoBehaviour
{
    private static CustomSceneManager _instance;

    public static CustomSceneManager GetInstance()
    {
        if (_instance == null) throw new NullReferenceException();
        
        return _instance;
    }

    private void Awake()
    {
        if (_instance == null) _instance = this;
    }
    
    public void ChangeScene(string sceneName)
    {
        if (SceneManager.GetActiveScene().name.Equals(sceneName) || sceneName.Length <= 0)
        {
            throw new ArgumentException();
        }

        StartCoroutine(LoadSceneAsync(sceneName));
    }

    public void Quit()
    {
        Application.Quit();
    }
    
    private IEnumerator LoadSceneAsync(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
    
}

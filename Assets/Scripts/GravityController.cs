﻿using DG.Tweening;
using UnityEngine;

public class GravityController : MonoBehaviour
{
    public Camera mainCamera;

    public GameObject beamPrefab;
    
    public SpriteRenderer[] spriteRenderers;

    private Vector3 _mousePos;
    
    private bool _enabled;
    private float _deltaTime;
    private float _timer = 1;
    
    private void FixedUpdate()
    {
        if (!_enabled && _deltaTime >= _timer) return;
        
        if (_enabled)
        {
            _mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            _mousePos.z = 0;
            _mousePos.y = 0;
        }
        
        if (_timer - _deltaTime <= 0)
        {
            _deltaTime = 0;
        }
        else
        {
            _deltaTime += Time.deltaTime;
        }

        transform.position = Vector3.Lerp(transform.position, _mousePos, _deltaTime);
    }

    public void SetEnabled(bool isEnabled)
    {
        if (_enabled == isEnabled) return;
        _enabled = isEnabled;
        
        if (_enabled) _deltaTime = 0;

        foreach (var sprite in spriteRenderers)
        {
            sprite.DOKill();
            sprite.DOFade(_enabled ? 1 : 0.3f, 0.5f);
        }
    }

    public void OnClick()
    {
        if (_enabled)
        {
            TrashMan.spawn(beamPrefab, _mousePos);
        }
    }
}

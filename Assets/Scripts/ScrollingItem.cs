﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ScrollingItem : MonoBehaviour
{
    public Rigidbody2D rb;

    private bool _isAdded;

    private bool _isInit;

    private bool _isQuitting;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    protected void Start()
    {
        if (!_isAdded) ScrollingManager.GetInstance().AddScrollingItemToList(this);
        _isAdded = true;
        _isInit = true;
    }

    private void OnApplicationQuit()
    {
        _isQuitting = true;
    }
    
    private void OnEnable()
    {
        if (!_isAdded && _isInit)
        {
            ScrollingManager.GetInstance().AddScrollingItemToList(this);
            _isAdded = true;
        }
    }
    
    private void OnDisable()
    {
        if (_isAdded && !_isQuitting) 
        {
            ScrollingManager.GetInstance().RemoveScrollingItemFromList(this);
            _isAdded = false;
        }
    }


    protected void OnBecameVisible()
    {
        if (!_isAdded) ScrollingManager.GetInstance().AddScrollingItemToList(this);
        _isAdded = true;
    }

    protected virtual void OnBecameInvisible()
    {
        if (_isQuitting) return;
        
        ScrollingManager.GetInstance().RemoveScrollingItemFromList(this);
        _isAdded = false;
    }
}
